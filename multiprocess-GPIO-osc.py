#!/usr/bin/env python
import OSC
import multiprocessing, time
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False) 

pin_pairs = [[14,15], [23,24], [1,7]]


# OSC
client = OSC.OSCClient()
address = '127.0.0.1', 3000 # 57120==SC
client.connect( address ) # set the address for all following messages
oscmsg = OSC.OSCMessage() # OSCresponder name: '/touch'
oscmsg.setAddress("/touch")
print client

def osc_msg(msg,  pinpair, val):
    pinindex = pin_pairs.index(pinpair)
    msg.append( ['pin pair',pinindex, str(pinpair),  val] )
    print msg
    client.send(msg)
    msg.clearData()

def discharge(a_pin, b_pin):
    GPIO.setup(a_pin, GPIO.IN)
    GPIO.setup(b_pin, GPIO.OUT)
    GPIO.output(b_pin, False)
    time.sleep(0.005)

def charge_time(a_pin, b_pin):
    GPIO.setup(b_pin, GPIO.IN)
    GPIO.setup(a_pin, GPIO.OUT)
    count = 0
    GPIO.output(a_pin, True)
    while not GPIO.input(b_pin):
        count = count + 1
    return count

def analog_read(pin_pair):
    discharge(pin_pair[0],pin_pair[1])
    return charge_time(pin_pair[0],pin_pair[1])


def worker(pair):
    """thread worker function"""
    while True:
        reading = analog_read( pair ) #print(analog_read())
        osc_msg(oscmsg, pair, reading)
        time.sleep(0.1)
                
    return

if __name__ == '__main__':
    jobs = []
    for i in range(len(pin_pairs)):
        p = multiprocessing.Process(target=worker, args=(pin_pairs[i],))
        jobs.append(p)
        p.start()
