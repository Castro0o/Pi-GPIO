# GPIO records touch (conductivity between) metal plates
[Wiki page](http://pzwiki.wdka.nl/mediadesign/Pushing_the_Score-publication#Pi_skin_conductivity)

`./multiprocess-GPIO-osc.py` **reads the GPIO values and sends OSC messages to 127.0.0.1 on port 3000.**
  
## What it should do:
* 3 pairs of GPIO pins are used as to capture skin conductivity  
* python reads GIOP (should run as a deamon)
* OSC sends messages from py to pd 

## run it
* In a pi terminal window run the pd patch: `./runpd.sh`
* In another pi terminal run the python script: `./multiprocess-GPIO-osc.py`


## files:
* multiprocess-GPIO-osc.py - python reading GPIO and sending OSC (future daemon)
* osc-test.pd - reads all OSC msgs send to port 3000
* osc-test.py - quick test `py ->(osc)-> pd` 
* test-GPIO.py - simple test of python reading 1 pair of GPIO
* osc-test.pd: pd patch that receives and prints OSC msgs on 127.0.0.1:3000
* runpd.sh - a convenience script to run pdheadless

to run pd headless
`pd-extended -nogui osc-test.pd` or `puredata -nogui osc-test.pd`

