import OSC
from time import sleep

client = OSC.OSCClient()
address = '127.0.0.1', 3000 # 57120==SC
client.connect( address ) # set the address for all following messages
print client

msg = OSC.OSCMessage() # OSCresponder name: '/touch'
msg.setAddress("/touch")
#msg.append('hello from python')
#client.send(msg)

for i in range(5): # 1006..1011
    synth_n = 1020+i
    msg.append( ['python says', i])
#    msg.extend( [ "/s_new", "player", synth_n, 'bufnum', i, 'vol',0  ])    
    print msg
    client.send(msg)
#    msg.clearData()
    sleep(float(1))

client.close()
