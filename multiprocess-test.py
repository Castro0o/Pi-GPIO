import multiprocessing, time

pin_pairs = [[18,23], [24,25], [22,17]]


def worker(pair):
    """thread worker function"""

    while True:
        print 'reading', pair #print(analog_read())
        time.sleep(0.1)
                
    return

if __name__ == '__main__':
    jobs = []
    for i in range(len(pin_pairs)):
        p = multiprocessing.Process(target=worker, args=(pin_pairs[i],))
        jobs.append(p)
        p.start()
