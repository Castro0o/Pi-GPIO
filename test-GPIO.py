import OSC
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

pin_pairs = [[14,15]]
pin_pair = pin_pairs[0]
a_pin = pin_pair[0]
b_pin = pin_pair[1]

allvals=[]

# touch resistance
# Max: 3
# Min: 273337

# OSC
client = OSC.OSCClient()
address = '127.0.0.1', 3000 # 57120==SC
client.connect( address ) # set the address for all following messages
oscmsg = OSC.OSCMessage() # OSCresponder name: '/touch'
oscmsg.setAddress("/touch")
print client

def osc_msg(msg,  val):
    msg.append( ['python sensor 01', val] )
    print msg
    client.send(msg)
    msg.clearData()

def discharge():
    GPIO.setup(a_pin, GPIO.IN)
    GPIO.setup(b_pin, GPIO.OUT)
    GPIO.output(b_pin, False)
    time.sleep(0.005)

def charge_time():
    GPIO.setup(b_pin, GPIO.IN)
    GPIO.setup(a_pin, GPIO.OUT)
    count = 0
    GPIO.output(a_pin, True)
    while not GPIO.input(b_pin):
        count = count + 1
    return count

def analog_read():
    discharge()
    return charge_time()

def summary():
    allvals.sort()
    max_val = allvals[0]
    min_val = allvals[-1]
    print 'Max:', max_val
    print 'Min:',min_val

    
try:
    while True:
        reading = analog_read()
        allvals.append(reading)
        print(reading)
        osc_msg(oscmsg, reading)
        time.sleep(0.1)        
except KeyboardInterrupt:
    print 'clean'
    GPIO.cleanup()
    client.close()
